package com.brisantecomp.datatype.example;

import java.math.BigDecimal;
import java.util.ArrayList;

public class NotPrimitiveExamples {


	public static void main(String[] args){
		executeExample();
	}
	
	public static void executeExample() {

		Integer integerNotPrimitive;
		Long longNotPrimitive;
		Double doubleNotPrimitive;
		BigDecimal bigDecimalNotPrimitive;
		Boolean booleanNotPrimitive;
		String stringNotPrimitive;
		
		String[] arrayString;
		Integer[] arrayInteger;

		ArrayList<String> arrayListString;
		ArrayList<PersonClassExample> arrayListPersonExample;
		
		
		System.out.println("los tipo de datos No primitivos tienen que ser instanciados");
		System.out.println("solo pueden asignarseles un valor o ser instanciados para poder ser usados ");

		//formas de asignar un valor Integer
		integerNotPrimitive = new Integer(0);
		integerNotPrimitive = 0;
		integerNotPrimitive = Integer.valueOf(0);
		integerNotPrimitive = Integer.valueOf("0");

		//formas de asignar un valor Long 
		longNotPrimitive = new Long(0);
		longNotPrimitive = 0L;
		longNotPrimitive = Long.valueOf(0);
		longNotPrimitive = Long.valueOf("0");

		//formas de asignar un valor Double 
		doubleNotPrimitive = new Double(0);
		doubleNotPrimitive = 0D;
		doubleNotPrimitive = Double.valueOf(0);
		doubleNotPrimitive = Double.valueOf("0");
		doubleNotPrimitive = Double.valueOf(10.5);
		doubleNotPrimitive = Double.valueOf("15.5");
		
		//al instancearse de esta forma se le puede asignar un valor entero o decimal
		bigDecimalNotPrimitive = new BigDecimal(0);
		bigDecimalNotPrimitive = new BigDecimal("0");
		bigDecimalNotPrimitive = new BigDecimal(0.5);
		bigDecimalNotPrimitive = new BigDecimal("10.05");
		//al parsearse de esta forma solo se puede asignar un valor entero
		bigDecimalNotPrimitive = new BigDecimal(Integer.valueOf(0));
		bigDecimalNotPrimitive = new BigDecimal(Long.valueOf(0));
		bigDecimalNotPrimitive = BigDecimal.valueOf(0);
		bigDecimalNotPrimitive = BigDecimal.valueOf(Long.valueOf(0));
		
		//formas de asignar un valor Boolean 
		booleanNotPrimitive = false;
		booleanNotPrimitive = new Boolean("true");
		booleanNotPrimitive = Boolean.FALSE;
		booleanNotPrimitive = Boolean.valueOf("false");

		//formas de asignar un valor String 
		stringNotPrimitive = "";
		stringNotPrimitive = new String("");
		stringNotPrimitive = String.valueOf("");
		stringNotPrimitive = bigDecimalNotPrimitive.toString();
		stringNotPrimitive = booleanNotPrimitive.toString();
		stringNotPrimitive = doubleNotPrimitive.toString();
		stringNotPrimitive = longNotPrimitive.toString();
		stringNotPrimitive = integerNotPrimitive.toString();
		
		
		//Primera forma de asignar valores a un array
		arrayString = new String[]{"Valor 1","Valor 2","Valor 3"};
		arrayInteger = new Integer[] { 5, 7, 9 };

		//Segunda forma de asignar valores a un array
		arrayString = new String[2];
		arrayString[0] = "Valor aleatorio";
		arrayString[1] = "Segundo valor";
		//arrayString.length; //retorna el tamano de la lista
		
		System.out.println("para conocer el valor, basta con indicar las posicion del contenido arrayString[1] :"+arrayString[1]);

		arrayInteger = new Integer[2];
		arrayInteger[0] = 2;
		arrayInteger[1] = 8;
		//arrayInteger.length; //retorna el tamano de la lista
		System.out.println("para conocer el valor, basta con indicar las posicion del contenido arrayInteger[0] :"+arrayInteger[0]);
		
		//Primera forma de hacer un FOR
		for(int i=0; i<arrayString.length; i++ ) {
			System.out.println(" valor del array arrayString["+i+"]: "+arrayString[i]);
		}

		//Segunda forma de hacer un FOR
		int contadorSoloParaSaberEnQueVueltaEstoy = 0;
		for(String contenidoArray : arrayString ) {
			System.out.println(" valor de la variable contenidoArray en esta vuelta ("+contadorSoloParaSaberEnQueVueltaEstoy+") es : "+contenidoArray);
			contadorSoloParaSaberEnQueVueltaEstoy++;
		}

		arrayListString = new ArrayList<String>();
		arrayListString.add("valor uno"); //le asigna la posicion 0
		arrayListString.add("segundo valor"); //le asigna la posicion 1
		arrayListString.size(); //retorna el tamano de la lista
		
		arrayListPersonExample = new ArrayList<PersonClassExample>();
		
		arrayListPersonExample.add(new PersonClassExample(1,"Jhonatan Harold", 1 , "45639810"));	//posicion 0
		
		PersonClassExample examplePerson = new PersonClassExample();
		examplePerson.setIdPerson(2);
		examplePerson.setFullname("Andersson Jair");
		examplePerson.setDocumentType(1);
		examplePerson.setDocumentNumber("8998751");
		arrayListPersonExample.add(examplePerson);	//posicion 1

		arrayListPersonExample.add(new PersonClassExample());	//posicion 2
		arrayListPersonExample.get(2).setIdPerson(3);
		arrayListPersonExample.get(2).setFullname("Samantha Rachel");
		arrayListPersonExample.get(2).setDocumentType(1);
		arrayListPersonExample.get(2).setDocumentNumber("9665147");
		
		for(int i=0; i<arrayListPersonExample.size(); i++ ) {
			PersonClassExample personaEnCadaVuelta = arrayListPersonExample.get(i);
			System.out.println(" arrayListPersonExample en la vuelta: "+i+" => "+personaEnCadaVuelta.toString());
		}

		//busqueda por Numero de Documento
		String numeroDeDocumentoAComparar = "8998751";
		for(int i=0; i<arrayListPersonExample.size(); i++ ) {
			PersonClassExample personaEnCadaVuelta = arrayListPersonExample.get(i);
			if(personaEnCadaVuelta.getDocumentNumber().equals(numeroDeDocumentoAComparar) ) {
				System.out.println(" se encontro el numero documento ("+numeroDeDocumentoAComparar+") arrayListPersonExample en la vuelta: "+i+" => "+personaEnCadaVuelta.toString());
			}
		}
		
		
		System.out.println("intNotPrimitive: "+integerNotPrimitive);
		System.out.println("longNotPrimitive: "+longNotPrimitive);
		System.out.println("doubleNotPrimitive: "+doubleNotPrimitive);
		System.out.println("bigDecimalNotPrimitive: "+bigDecimalNotPrimitive);
		System.out.println("booleanNotPrimitive: "+booleanNotPrimitive);
		System.out.println("stringNotPrimitive: "+stringNotPrimitive);
		
	}

}
