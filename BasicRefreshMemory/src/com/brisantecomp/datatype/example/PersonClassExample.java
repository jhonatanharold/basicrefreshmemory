package com.brisantecomp.datatype.example;

import java.io.Serializable;

public class PersonClassExample implements Serializable{

	private static final long serialVersionUID = 1L;
	//siempre debe implementar de serializable

	private Integer idPerson;
	private String fullname;
	private Integer documentType;
	private String documentNumber;
	
	//constructor simple
	public PersonClassExample() {
		
	}
	
	//constructor con parametros - este asigna el valor de un parametro a una variable privada
	public PersonClassExample(Integer idPerson, String fullname, Integer documentType,
			String documentNumber) {
		super();
		this.idPerson = idPerson;
		this.fullname = fullname;
		this.documentType = documentType;
		this.documentNumber = documentNumber;
	}

	//los get obtienen los valores de las variables privadas
	public Integer getIdPerson() {
		return idPerson;
	}

	//los set asignan un dato en las variables privadas a partir del parametro enviado
	public void setIdPerson(Integer idPerson) {
		this.idPerson = idPerson;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public Integer getDocumentType() {
		return documentType;
	}

	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	//un metodo que devuelve todos los campos en formato String
	@Override
	public String toString() {
		return "PersonClassExample [idPerson=" + idPerson + ", fullname=" + fullname + 
			", documentType=" + documentType + ", documentNumber=" + documentNumber + "]";
	}
	
}
