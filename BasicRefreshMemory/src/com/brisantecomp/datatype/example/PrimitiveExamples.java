package com.brisantecomp.datatype.example;

public class PrimitiveExamples {


	public static void main(String[] args){
		executeExample();
	}
	
	public static void executeExample() {

		int intPrimitive;
		long longPrimitive;
		double doublePrimitive;
		boolean booleanPrimitive;
		
		System.out.println("los tipo de datos primitivos no pueden asignarseles nulo");
		System.out.println("solo pueden asignarseles un valor para poder ser usados ");
		
		intPrimitive = 0;
		longPrimitive = 0;
		doublePrimitive = 0;
		booleanPrimitive = false;
		
		System.out.println("intPrimitive: "+intPrimitive);
		System.out.println("longPrimitive: "+longPrimitive);
		System.out.println("doublePrimitive: "+doublePrimitive);
		System.out.println("booleanPrimitive: "+booleanPrimitive);
		
		
	}

}
