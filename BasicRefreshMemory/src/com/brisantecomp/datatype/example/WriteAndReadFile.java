package com.brisantecomp.datatype.example;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;


public class WriteAndReadFile {


	public static void main(String[] args){
		
		escribirEnArchivo();
		escribirEnArchivoConUnaClase();
		
		leerEnArchivo();
		leerEnArchivoConUnaClase();
	}

	//EJEMPLO 1
	public static void escribirEnArchivo() {
		
		FileWriter fichero = null;//Clase con la que creo un fichero, puedo especificarle una ruta/nombredelarchivo.txt
		PrintWriter pw = null;//Clase con la que se escribe en el archivo
		
		ArrayList<String> lineFileArray = new ArrayList<String>();
		lineFileArray.add("Escribo en la Linea 1");
		lineFileArray.add("Escribo en la Linea 2");
		lineFileArray.add("Escribo en la Linea 3");
		
		try
		{
			//Inicializo la clase FileWriter y esta crea el archivo
		    fichero = new FileWriter("prueba.txt");
		    
		    //Inicializo la clase PrintWriter con la que escribire
		    pw = new PrintWriter(fichero);
		    
		    pw.print("Escribo En una Linea \n\n");//Escribo en el archivo => pw.print
		
		    //uso un arreglo de String para escribir
		    for (int i = 0; i < lineFileArray.size(); i++) {
		    	pw.println(lineFileArray.get(i));//Escribo linea a linea => pw.println
		    }

		    //La forma de usar el PrintWriter es similar a la que se usa para escribir en consola:
		    //System.out.print(" Escribo en consola ");
		    //System.out.println("Escribo linea a linea en consola ");
		} catch (Exception e) {
		    e.printStackTrace();
		} finally {
		   try {
		   // Nuevamente aprovechamos el finally para 
		   // asegurarnos que se cierra el fichero.
		       if (null != fichero) {
		          fichero.close();
		      }
		       System.out.println("Se ejecuto la funcion escribirEnArchivo");
		       System.out.println("----------------------------------------");
		   } catch (Exception e2) {
		      e2.printStackTrace();
		   }
		}
		
	}
	

	//EJEMPLO 2
	public static void escribirEnArchivoConUnaClase() {
		List<PersonClassExample> listaPersonasExample = new ArrayList<PersonClassExample>();
		PersonClassExample personaExample = new PersonClassExample();
		personaExample.setIdPerson(1);
		personaExample.setDocumentNumber("45639810");
		personaExample.setDocumentType(1);
		personaExample.setFullname("Jhonatan Ovalle");
		listaPersonasExample.add(personaExample);
	
		personaExample = new PersonClassExample();
		personaExample.setIdPerson(2);
		personaExample.setDocumentNumber("55879823");
		personaExample.setDocumentType(1);
		personaExample.setFullname("Andersson Ovalle");
		listaPersonasExample.add(personaExample);
		

		FileWriter fichero = null;
		PrintWriter pw = null;
		try{
			
		    fichero = new FileWriter("baseDeDatosPersonas.txt");
		    pw = new PrintWriter(fichero);
		    
		    String strPersona = "";
		    for (int i = 0; i < listaPersonasExample.size(); i++) {
		    	strPersona += listaPersonasExample.get(i).getIdPerson()+",";
		    	strPersona += listaPersonasExample.get(i).getDocumentNumber()+",";
		    	strPersona += listaPersonasExample.get(i).getDocumentType()+",";
		    	strPersona += listaPersonasExample.get(i).getFullname();
		    	pw.println(strPersona);
		    	strPersona = "";
		    }

		} catch (Exception e) {
		    e.printStackTrace();
		} finally {
		   try {
		   // Nuevamente aprovechamos el finally para 
		   // asegurarnos que se cierra el fichero.
		       if (null != fichero) {
		          fichero.close();
		      }
		       System.out.println("Se ejecuto la funcion escribirEnArchivoConUnaClase");
		       System.out.println("----------------------------------------");
		   } catch (Exception e2) {
		      e2.printStackTrace();
		   }
		}
		
	}
	
	
	public static void leerEnArchivo() {
		FileReader fileReader = null;
		
	      try {
	          //con la clase File especifico la ruta/nombreArchivo.txt del archivo que quiero leer
	    	  File filePath = new File ("prueba.txt");
	    	  //con la clase FileReader leer el contenido del archivo
	    	  fileReader = new FileReader (filePath);
	    	  //En el BufferReader almacenare en memoria lo que el FileReader acaba de leer
	    	  BufferedReader bufferedReader = new BufferedReader(fileReader);
	    	  
	          String linea;
	          System.out.println("El texto que voy a leer es: ");
	    	  while( (linea = bufferedReader.readLine()) != null ) {
		    	  System.out.println(linea);
		      }
	    	  
	         
	      }catch(Exception e){
	         e.printStackTrace();
	      }finally{
	         // En el finally cerramos el ficheron.
	         try{                    
	            if( null != fileReader ){   
	               fileReader.close();     
	            }                  
	         }catch (Exception e2){ 
	            e2.printStackTrace();
	         }
	         System.out.println("Se ejecuto la funcion de leerEnArchivo");
	         System.out.println("----------------------------------------");
	      }
	   
	}
	
	
	public static void leerEnArchivoConUnaClase() {
		FileReader fileReader = null;
		
	      try {
	          //con la clase File especifico la ruta/nombreArchivo.txt del archivo que quiero leer
	    	  File filePath = new File ("baseDeDatosPersonas.txt");
	    	  //con la clase FileReader leer el contenido del archivo
	    	  fileReader = new FileReader (filePath);
	    	  //En el BufferReader almacenare en memoria lo que el FileReader acaba de leer
	    	  BufferedReader bufferedReader = new BufferedReader(fileReader);
	    	  
	    	  //Creo una lista de String que guardara linea a linea lo que se encuentra en el archivo
	    	  List<String> listaDeTextoLineaALinea= new ArrayList<String>();
	    	  
	          String linea;
	    	  while( (linea = bufferedReader.readLine()) != null ) {
	    		  listaDeTextoLineaALinea.add(linea);
		      }
	    	  

	  		List<PersonClassExample> listaPersonasExample = new ArrayList<PersonClassExample>();
	  		  
	    	  for(int i=0; i< listaDeTextoLineaALinea.size(); i++) {
	    		  //la funcion split crea un array de String, solo hay que colocarle el separador, para este caso es la "," que es como se definio en la escritura del archivo baseDeDatosPersonas.txt
	    		  String[] arrayLineaSeparadoPorComa = listaDeTextoLineaALinea.get(i).split(",");

	    		  Integer idPersona = Integer.valueOf(arrayLineaSeparadoPorComa[0]);
	    		  String documentNumber = arrayLineaSeparadoPorComa[1];
	    		  Integer documentType = Integer.valueOf(arrayLineaSeparadoPorComa[2]);
	    		  String fullName = arrayLineaSeparadoPorComa[3];
	    		  
	    		  PersonClassExample personaExample = new PersonClassExample();
		  	  	  personaExample.setIdPerson(idPersona);
		  	  	  personaExample.setDocumentNumber(documentNumber);
		  	  	  personaExample.setDocumentType(documentType);
		  	  	  personaExample.setFullname(fullName);
		  	  	  listaPersonasExample.add(personaExample);
		  	  	  System.out.println(personaExample.toString());
	    	  }
	    	  System.out.println(">> la variable listaPersonasExample tiene "+listaPersonasExample.size()+" registros");
	    	  
		    	
	      }catch(Exception e){
	         e.printStackTrace();
	      }finally{
	         // En el finally cerramos el ficheron.
	         try{                    
	            if( null != fileReader ){   
	               fileReader.close();     
	            }                  
	         }catch (Exception e2){ 
	            e2.printStackTrace();
	         }
	         System.out.println("Se ejecuto la funcion de leerEnArchivoConUnaClase");
	         System.out.println("----------------------------------------");
	      }
	   
	}
	
}
